<?php
/**
 * @package 	WordPress
 * @subpackage 	Total School Child
 * @version		1.0.0
 *
 * Child Theme Functions File
 * Created by CMSMasters
 *
 */


function total_school_enqueue_styles() {
    $parent_style = 'theme-style';


    wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );

    wp_enqueue_style('child-theme-style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
    wp_enqueue_style( 'swiper', get_template_directory_uri() . '/../total-school-child/dist/swiper/css/swiper.min.css', array());

    wp_enqueue_script( 'swipper', get_template_directory_uri() . '/../total-school-child/dist/swiper/js/swiper.min.js', array(), '', true );
    wp_enqueue_script( 'appai', get_template_directory_uri() . '/../total-school-child/js/javascript.js', array(), '', true );

}

add_action('wp_enqueue_scripts', 'total_school_enqueue_styles');
add_filter( 'style_loader_src',  'sdt_remove_ver_css_js', 9999, 2 );
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999, 2 );

function sdt_remove_ver_css_js( $src, $handle )
{
    $handles_with_version = [ 'style' ]; // <-- Adjust to your needs!

    if ( strpos( $src, 'ver=' ) && ! in_array( $handle, $handles_with_version, true ) )
        $src = remove_query_arg( 'ver', $src );

    return $src;
}

?>
