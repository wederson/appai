jQuery( document ).ready(function() {
  var appendNumber = 4;
  var prependNumber = 1;

  if (jQuery( window ).width() <= 768 ) {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 2,
        spaceBetween: 30,
        loop: true
    });
  }
  if(jQuery( window ).width() <= 460 ) {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true
    });
  } else {
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        spaceBetween: 30,
        loop: true
    });

  }


  jQuery(".submit-search").click(function(){
    jQuery("#form-appai").submit();
  });
});
