<?php
/**
 * @package 	WordPress
 * @subpackage 	Total School
 * @version 	1.0.0
 * 
 * Website Events Functions
 * Created by CMSMasters
 * 
 */


/* Replace Styles */
function total_school_replace_tribe_events_calendar_stylesheet() {
	$styleUrl = '';
	
	
	return $styleUrl;
}

add_filter('tribe_events_stylesheet_url', 'total_school_replace_tribe_events_calendar_stylesheet');


/* Replace Pro Styles */
function total_school_replace_tribe_events_calendar_pro_stylesheet() {
	$styleUrl = '';
	
	
	return $styleUrl;
}

add_filter('tribe_events_pro_stylesheet_url', 'total_school_replace_tribe_events_calendar_pro_stylesheet');


/* Replace Widget Styles */
function total_school_replace_tribe_events_calendar_widget_stylesheet() {
	$styleUrl = '';
	
	
	return $styleUrl;
}

add_filter('tribe_events_pro_widget_calendar_stylesheet_url', 'total_school_replace_tribe_events_calendar_widget_stylesheet');


/* Replace Responsive Styles */
function total_school_customize_tribe_events_breakpoint() {
    return 749;
}

add_filter('tribe_events_mobile_breakpoint', 'total_school_customize_tribe_events_breakpoint');


/* Get Previous & Next Events Links Function */
function total_school_prev_next_events() {
	$cmsmasters_event_type = get_post_type();
	
	$published_events = wp_count_posts($cmsmasters_event_type)->publish;
	
	if ($published_events > 1) {
		echo '<aside id="tribe-events-sub-nav" class="post_nav">' . 
			'<span class="tribe-events-nav-previous cmsmasters_prev_post post_nav_prev_arrow">'; 
				
				tribe_the_prev_event_link('%title%');
				
			echo '</span>' . 
			'<span class="tribe-events-nav-next cmsmasters_next_post post_nav_next_arrow">';
				
				tribe_the_next_event_link('%title%');
				
			echo '</span>' . 
		'</aside>';
	}
}


