<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 * 
 * @cmsmasters_package 	Total School
 * @cmsmasters_version 	1.0.0
 *
 */


if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


get_header();


list($cmsmasters_layout) = total_school_theme_page_layout_scheme();


echo '<!--_________________________ Start Content _________________________ -->' . "\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo '<div class="content entry">' . "\n\t";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo '<div class="content entry fr">' . "\n\t";
} else {
	echo '<div class="middle_content entry">';
}

$tribe_page_class = 'cmsmasters_event_page_'; 

if (CMSMASTERS_EVENTS_CALENDAR && tribe_is_list_view()) {
	$tribe_page_class .= 'list'; 
} else if (CMSMASTERS_EVENTS_CALENDAR && tribe_is_month()) {
	$tribe_page_class .= 'month'; 
} else if (CMSMASTERS_EVENTS_CALENDAR && tribe_is_day()) {
	$tribe_page_class .= 'day'; 
} else if (CMSMASTERS_EVENTS_CALENDAR && function_exists('tribe_is_week') && tribe_is_week()) {
	$tribe_page_class .= 'week'; 
} else if (CMSMASTERS_EVENTS_CALENDAR && function_exists('tribe_is_map') && tribe_is_map()) {
	$tribe_page_class .= 'map'; 
} else if (CMSMASTERS_EVENTS_CALENDAR && function_exists('tribe_is_photo') && tribe_is_photo()) {
	$tribe_page_class .= 'photo'; 
} else {
	$tribe_page_class = ''; 
}

echo '<div id="tribe-events-pg-template" class="' . $tribe_page_class . ' clearfix">' . "\n\t";
	tribe_events_before_html();
	tribe_get_view();
	tribe_events_after_html();
	echo '<div class="cl"></div>';
echo '</div> <!-- #tribe-events-pg-template -->' . "\n";


echo '</div>' . "\n" . 
'<!-- _________________________ Finish Content _________________________ -->' . "\n\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo "\n" . '<!-- _________________________ Start Sidebar _________________________ -->' . "\n" . 
	'<div class="sidebar">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- _________________________ Finish Sidebar _________________________ -->' . "\n";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo "\n" . '<!-- _________________________ Start Sidebar _________________________ -->' . "\n" . 
	'<div class="sidebar fl">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- _________________________ Finish Sidebar _________________________ -->' . "\n";
}


get_footer();

