/**
 * @package 	WordPress
 * @subpackage 	Total School
 * @version 	1.0.1
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.


-----------------------------------------------------------------------
				FILE LOGS
-----------------------------------------------------------------------


Version 1.0.1: files operations:

	Theme Files removed:
	
		total-school\learnpress\addons\course-review\add-review.php
		total-school\learnpress\addons\course-review\loop-review.php
		total-school\learnpress\addons\course-wishlist\user-wishlist.php
		total-school\learnpress\addons\course-wishlist\wishlist-content.php
		total-school\learnpress\archive-course-content.php
		total-school\learnpress\archive-course.php
		total-school\learnpress\cmsmasters-learnpress\categories_archive.php
		total-school\learnpress\cmsmasters-learnpress\categories_single.php
		total-school\learnpress\cmsmasters-learnpress\duration_single.php
		total-school\learnpress\cmsmasters-learnpress\enroll_button_single.php
		total-school\learnpress\cmsmasters-learnpress\instructor_archive.php
		total-school\learnpress\cmsmasters-learnpress\instructor_single.php
		total-school\learnpress\cmsmasters-learnpress\landing_sidebar.php
		total-school\learnpress\cmsmasters-learnpress\learning_sidebar.php
		total-school\learnpress\cmsmasters-learnpress\price_archive.php
		total-school\learnpress\cmsmasters-learnpress\price_single.php
		total-school\learnpress\cmsmasters-learnpress\rate_archive.php
		total-school\learnpress\cmsmasters-learnpress\rate_single.php
		total-school\learnpress\cmsmasters-learnpress\status_single.php
		total-school\learnpress\cmsmasters-learnpress\students_single.php
		total-school\learnpress\cmsmasters-learnpress\tags_single.php
		total-school\learnpress\cmsmasters-learnpress\wishlist_button_single.php
		total-school\learnpress\content-single-course.php
		total-school\learnpress\content-single-quiz.php
		total-school\learnpress\lesson\navigation.php
		total-school\learnpress\lesson\next-button.php
		total-school\learnpress\lesson\prev-button.php
		total-school\learnpress\profile\info.php
		total-school\learnpress\single-course.php
		total-school\learnpress\single-course\content-landing.php
		total-school\learnpress\single-course\content-learning.php
		total-school\learnpress\single-course\progress.php
		total-school\learnpress\single-course\section\item-lesson.php
		total-school\learnpress\single-course\section\item-quiz.php
		total-school\learnpress\single-quiz.php
	
	
	Theme Files added:
	
		total-school\single-lp_lesson.php
	
	
	Theme Files edited:
	
		total-school\css\adaptive.css
		total-school\css\fontello-custom.css
		total-school\css\fonts\fontello-custom.eot
		total-school\css\fonts\fontello-custom.svg
		total-school\css\fonts\fontello-custom.ttf
		total-school\css\fonts\fontello-custom.woff
		total-school\css\less\adaptive.less
		total-school\css\less\style.less
		total-school\framework\admin\inc\fonts\config-custom.json
		total-school\framework\admin\inc\plugin-activator.php
		total-school\framework\admin\inc\plugins\cmsmasters-content-composer.zip
		total-school\framework\admin\inc\plugins\LayerSlider.zip
		total-school\framework\admin\inc\plugins\revslider.zip
		total-school\framework\admin\options\cmsmasters-theme-options-post.php
		total-school\framework\function\template-functions-profile.php
		total-school\framework\function\template-functions.php
		total-school\framework\function\theme-functions.php
		total-school\framework\postType\blog\page\default\gallery.php
		total-school\framework\postType\blog\page\masonry\gallery.php
		total-school\framework\postType\blog\page\timeline\gallery.php
		total-school\framework\postType\blog\post\gallery.php
		total-school\framework\postType\blog\post\standard.php
		total-school\framework\postType\portfolio\post\gallery.php
		total-school\framework\postType\portfolio\post\standard.php
		total-school\framework\postType\quote\grid.php
		total-school\framework\postType\quote\slider.php
		total-school\functions.php
		total-school\learnpress\cmsmasters-framework\admin\plugin-options.php
		total-school\learnpress\cmsmasters-framework\admin\plugin-settings.php
		total-school\learnpress\cmsmasters-framework\css\less\plugin-adaptive.less
		total-school\learnpress\cmsmasters-framework\css\less\plugin-style.less
		total-school\learnpress\cmsmasters-framework\css\plugin-adaptive.css
		total-school\learnpress\cmsmasters-framework\css\plugin-rtl.css
		total-school\learnpress\cmsmasters-framework\css\plugin-style.css
		total-school\learnpress\cmsmasters-framework\function\plugin-colors.php
		total-school\learnpress\cmsmasters-framework\function\plugin-fonts.php
		total-school\learnpress\cmsmasters-plugin-functions.php
		total-school\page.php
		total-school\readme.txt
		total-school\style.css
		total-school\tribe-events\pro\single-organizer.php
		total-school\tribe-events\pro\single-venue.php



--------------------------------------
Version 1.0: Release!
