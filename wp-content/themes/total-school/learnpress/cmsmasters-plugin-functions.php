<?php
/**
 * @package 	WordPress
 * @subpackage 	Total School
 * @version 	1.0.1
 * 
 * LearnPress Functions
 * Created by CMSMasters
 * 
 */


/* Load Parts for LearnPress Plugin */
locate_template('learnpress/cmsmasters-framework/function/plugin-colors.php', true);

locate_template('learnpress/cmsmasters-framework/function/plugin-fonts.php', true);

locate_template('learnpress/cmsmasters-framework/admin/plugin-settings.php', true);

locate_template('learnpress/cmsmasters-framework/admin/plugin-options.php', true);



/* Register CSS Styles and Scripts for LearnPress Plugin */
function total_school_learnpress_register_styles_scripts() {
	$depend_learnpress_style = array();
	
	if (LP()->settings->get('load_css') == 'yes' || LP()->settings->get('load_css') == '') {
		$depend_learnpress_style = array('learn-press-style');
	}
	
	wp_enqueue_style('cmsmasters-learnpress-style', get_template_directory_uri() . '/learnpress/cmsmasters-framework/css/plugin-style.css', $depend_learnpress_style, '1.0.0', 'screen');
	
	wp_enqueue_style('cmsmasters-learnpress-adaptive', get_template_directory_uri() . '/learnpress/cmsmasters-framework/css/plugin-adaptive.css', $depend_learnpress_style, '1.0.0', 'screen');
	
	
	if (is_rtl()) {
		wp_enqueue_style('cmsmasters-learnpress-rtl', get_template_directory_uri() . '/learnpress/cmsmasters-framework/css/plugin-rtl.css', $depend_learnpress_style, '1.0.0', 'screen');
	}
}

add_action('wp_enqueue_scripts', 'total_school_learnpress_register_styles_scripts');



/* Developer Mode for LearnPress Plugin */
function total_school_learnpress_developer_mode() {
	if (!is_admin() && CMSMASTERS_DEVELOPER_MODE) {
		wp_dequeue_style('cmsmasters-learnpress-style');
		
		wp_dequeue_style('cmsmasters-learnpress-adaptive');
		
		echo '<link rel="stylesheet/less" href="' . get_template_directory_uri() . '/learnpress/cmsmasters-framework/css/less/plugin-style.less" type="text/css" media="screen" />';
		
		echo '<link rel="stylesheet/less" href="' . get_template_directory_uri() . '/learnpress/cmsmasters-framework/css/less/plugin-adaptive.less" type="text/css" media="screen" />';
	}
}

add_action('wp_enqueue_scripts', 'total_school_learnpress_developer_mode');



/* Register Post Formats for LearnPress Plugin */
if (function_exists('add_theme_support')) {
	function total_school_lpr_course_add_attr() {
		add_post_type_support('lp_course', 'page-attributes');
	}
	
	add_action('init', 'total_school_lpr_course_add_attr', 100);
}

