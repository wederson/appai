<?php
/**
 * @package 	WordPress
 * @subpackage 	Total School
 * @version 	1.0.1
 * 
 * LearnPress Fonts Rules
 * Created by CMSMasters
 * 
 */


function total_school_learnpress_fonts($custom_css) {
	$cmsmasters_option = total_school_get_global_options();
	
	
	$custom_css .= "
/***************** Start LearnPress Font Styles ******************/

	/* Start Content Font */
	.learn-press-question-options > li.answer-option > label p.single-lines,
	.review-form .review-fields > li > label,
	.review-form .review-fields > li > label .required,
	.learn-press-tabs.ui-widget,
	#review .review-fields > li > label .required,
	#review .review-fields > li > label {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_content_font_google_font']) . $cmsmasters_option['total-school' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['total-school' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_content_font_font_style'] . ";
	}
	
	.learn-press-courses li.course .course-info *,
	.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_footer,
	.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_footer a {
		font-size:" . ((int) $cmsmasters_option['total-school' . '_content_font_font_size'] - 1) . "px;
	}
	
	.lp-label  {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_content_font_google_font']) . $cmsmasters_option['total-school' . '_content_font_system_font'] . ";
		font-size:11px;
		line-height:20px;
		font-weight:bold;
		font-style:" . $cmsmasters_option['total-school' . '_content_font_font_style'] . ";
		text-transform:none;
	}
	/* Finish Content Font */


	/* Start H1 Font */
	.counter-block .counter .number,
	.cmsmasters_course_title {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_h1_font_google_font']) . $cmsmasters_option['total-school' . '_h1_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_h1_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['total-school' . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['total-school' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['total-school' . '_h1_font_text_decoration'] . ";
	}
	
	.counter-block .counter .number {
		font-size:" . ((int) $cmsmasters_option['total-school' . '_h1_font_font_size'] + 8) . "px;
		line-height:" . ((int) $cmsmasters_option['total-school' . '_h1_font_line_height'] + 14) . "px;
	}
	/* Finish H1 Font */


	/* Start H2 Font */
	#quiz-countdown,
	.students-list-title,
	.learn-press-content-item-title.content-item-quiz-title h4,
	.lp-course-progress-heading,
	.course-description-heading,
	#user_courses > h3,
	.course-review-head,
	.course-content > h3,
	.single-quiz .quiz-questions > h3,
	.course-curriculum-title {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_h2_font_google_font']) . $cmsmasters_option['total-school' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['total-school' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['total-school' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['total-school' . '_h2_font_text_decoration'] . ";
	}
	/* Finish H2 Font */
	
	
	/* Start H5 Font */
	.course-reviews-list li .review-title {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_h5_font_google_font']) . $cmsmasters_option['total-school' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['total-school' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['total-school' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['total-school' . '_h5_font_text_decoration'] . ";
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	#lp-navigation .nav-link .course-item-title,
	.recent-courses-widget .course-title, 
	.popular-courses-widget .course-title, 
	.featured-courses-widget .course-title,
	.learn-press-courses li.course h3,
	.course-students-list .students .name,
	.single-lp_course .learn-press-course-results-progress .number, 
	.single-lp_course .learn-press-course-results-progress .percentage-sign,
	.quiz-history thead th,
	.order_details thead tr th,
	.table-orders thead tr th,
	.learn-press-cart-table thead tr th,
	.learn-press-checkout-review-order-table tr th,
	.learn-press-tabs li a,
	.single-quiz .quiz-question-nav .lp-question-wrap > h4,
	.quiz-result .quiz-result-summary .quiz-result-field > label,
	.curriculum-sections .section-content .course-lesson, 
	.curriculum-sections .section-content .course-lesson a, 
	.curriculum-sections .section-content .course-quiz,
	.curriculum-sections .section-content .course-quiz a,
	.user-basic-info .user-nicename {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_h6_font_google_font']) . $cmsmasters_option['total-school' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['total-school' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['total-school' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['total-school' . '_h6_font_text_decoration'] . ";
	}
	
	#learn-press-course-curriculum .course-item.course-lesson:before, 
	#learn-press-course-curriculum .course-item.course-quiz:before {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_h6_font_google_font']) . $cmsmasters_option['total-school' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_h6_font_font_size'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['total-school' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['total-school' . '_h6_font_text_decoration'] . ";
	}
	/* Finish H6 Font */
	
	
	/* Srart Small Font */
	.quiz-countdown-label {
		font-family:" . total_school_get_google_font($cmsmasters_option['total-school' . '_small_font_google_font']) . $cmsmasters_option['total-school' . '_small_font_system_font'] . ";
		font-size:" . $cmsmasters_option['total-school' . '_small_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['total-school' . '_small_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['total-school' . '_small_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['total-school' . '_small_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['total-school' . '_small_font_text_transform'] . ";
	}
	/* Finish Small Font */
	

/***************** Finish LearPress Font Styles ******************/

";
	
	
	return $custom_css;
}

add_filter('total_school_theme_fonts_filter', 'total_school_learnpress_fonts');

