<?php
/**
 * @package 	WordPress
 * @subpackage 	Total School
 * @version 	1.0.1
 * 
 * LearnPress Colors Rules
 * Created by CMSMasters
 * 
 */


function total_school_learnpress_colors($custom_css) {
	$cmsmasters_option = total_school_get_global_options();
	
	
	$cmsmasters_color_schemes = cmsmasters_color_schemes_list();
	
	
	foreach ($cmsmasters_color_schemes as $scheme => $title) {
		$rule = (($scheme != 'default') ? "html .cmsmasters_color_scheme_{$scheme} " : 'html ');
		
		
		$custom_css .= "
/***************** Start {$title} LearnPress Color Scheme Rules ******************/

	/* Start Main Content Font Color */
	{$rule}.learn-press-pagination .page-numbers li .page-numbers,
	{$rule}.quiz-result .quiz-result-summary .quiz-result-field,
	{$rule}#learn-press-course-curriculum .course-item.item-has-status .item-status:before,
	{$rule}#learn-press-course-curriculum .course-item.item-has-status .item-status,
	{$rule}.learn-press-tabs .ui-widget-content,
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab a,
	{$rule}.learn-press-user-profile .learn-press-tabs > li > a {
		" . cmsmasters_color_css('color', $cmsmasters_option['total-school' . '_' . $scheme . '_color']) . "
	}
	/* Finish Main Content Font Color */
	
	
	/* Start Primary Color */
	{$rule}.learn-press-question-options > li.answer-option.answer-true label p, 
	{$rule}.learn-press-question-options > li.answer-option label input[type=checkbox] + p:after, 
	{$rule}.quiz-result .quiz-grade p span.passed, 
	{$rule}.learn-press-courses li.course .course-price, 
	{$rule}.recent-courses-widget .course-title:hover, 
	{$rule}.popular-courses-widget .course-title:hover, 
	{$rule}.featured-courses-widget .course-title:hover, 
	{$rule}.learn-press-pagination .page-numbers li .page-numbers:hover,
	{$rule}.learn-press-pagination .page-numbers li .page-numbers.current,
	{$rule}.learn-press-courses li.course h3:hover,
	{$rule}#learn-press-course-curriculum .course-item.item-has-status.item-completed .item-status,
	{$rule}#learn-press-course-curriculum .course-item.item-has-status.item-completed .item-status:before,
	{$rule}.course-students-list .students a.name:hover,
	{$rule}.complete-lesson-button.completed,
	{$rule}.question-results.correct .question-title:after, 
	{$rule}.learn-press-question-wrap.checked li.answer-true, 
	{$rule}.curriculum-sections .section-content > li [class^=\"cmsmasters_theme_icon_\"], 
	{$rule}.curriculum-sections .section-content > li [class*=\" cmsmasters_theme_icon_\"],
	{$rule}.single-quiz .quiz-result .quiz-result-summary .quiz-result-field.correct, 
	{$rule}.quiz-result .quiz-grade p span.failed,
	{$rule}#learn-press-course-curriculum .course-item.item-current:before,
	{$rule}.learn-press-subtabs li.current a,
	{$rule}.cart_item .remove,
	{$rule}.question-results.incorrect .question-title:after,
	{$rule}.learn-press-question-wrap.checked li.user-answer-false,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_cource_cat a,
	{$rule}.archive .lp_course .cmsmasters_lrp_archive_content .entry-footer .course-price,
	{$rule}.quiz-questions > ul li.current > h4,
	{$rule}.single-quiz .quiz-result .quiz-result-summary .quiz-result-field.wrong,
	{$rule}#review .review-fields > li > label .required {
		" . cmsmasters_color_css('color', $cmsmasters_option['total-school' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}#lp-navigation .nav-link .course-item-title:hover:before, 
	{$rule}.learn-press-question-options > li.answer-option label input[type=radio] + p:after, 
	{$rule}#popup_container #popup_title, 
	{$rule}.learn-press-pagination .page-numbers li .page-numbers:before, 
	{$rule}#course-curriculum-popup #popup-main #popup-header .sidebar-hide-btn:hover, 
	{$rule}#course-curriculum-popup #popup-main #popup-header .sidebar-show-btn:hover,
	{$rule}#course-curriculum-popup #popup-main #popup-header .popup-close:hover, 
	{$rule}#course-curriculum-popup #popup-main #popup-header, 
	{$rule}#popup_title,
	{$rule}.learn-press-checkout-review-order-table thead,
	{$rule}.learn-press-cart-table thead,
	{$rule}.order_details thead,
	{$rule}.table-orders thead,
	{$rule}.lp-course-progress.passed .lp-progress-value,
	{$rule}.quiz-history thead th,
	{$rule}.question-explanation-content:before,
	{$rule}.question-hint-content:before,
	{$rule}.lp-course-progress .lp-passing-conditional,
	{$rule}#user_courses > h3:before,
	{$rule}.lp-label,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_price,
	{$rule}.single-quiz .quiz-questions > h3:before,
	{$rule}.single-quiz .quiz-summary .entry-title:before,
	{$rule}.course-review-head:before,
	{$rule}.course-curriculum-title:before,
	{$rule}.cmsmasters_course_title:before,
	{$rule}.students-list-title:before,
	{$rule}.learn-press-content-item-title:before,
	{$rule}.course-lesson-heading:before,
	{$rule}.course-curriculum-title:before,
	{$rule}.lp-course-progress-heading:before, 
	{$rule}.learn-press-content-item-title.content-item-quiz-title .quiz-countdown:after,
	{$rule}.course-rate .review-bar .rating, 
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab:before, 
	{$rule}.learn-press-user-profile .learn-press-tabs > li:before, 
	{$rule}.learn-press-notice:before, 
	{$rule}.learn-press-message:before, 
	{$rule}.learn-press-error:before,
	{$rule}.lp-course-progress .lp-progress-bar .lp-progress-value,
	{$rule}.lp-label.lp-label-preview,
	{$rule}.lp-label-final,
	{$rule}#user_quizzes .quiz-result-mark:before,
	{$rule}.learn-press .message:before,
	{$rule}.learn-press .lp-message:before,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_free {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['total-school' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}#lp-navigation .nav-link .course-item-title:hover:before, 
	{$rule}#learn-press-block-content span:before, 
	{$rule}#course-curriculum-popup #popup-main #popup-header, 
	{$rule}#course-curriculum-popup #popup-main #popup-header .popup-menu {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['total-school' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.lp-course-progress .lp-passing-conditional:after {
		" . cmsmasters_color_css('border-bottom-color', $cmsmasters_option['total-school' . '_' . $scheme . '_link']) . "
	}
	
	{$rule}.lp-course-progress .lp-passing-conditional:before {
		" . cmsmasters_color_css('border-top-color', $cmsmasters_option['total-school' . '_' . $scheme . '_link']) . "
	}
	/* Finish Primary Color */
	
	
	/* Start Highlight Color */
	{$rule}.review-stars > li span.dashicons-star-empty:before,
	{$rule}.review-stars-rated .review-stars:before,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_cource_cat a:hover {
		" . cmsmasters_color_css('color', $cmsmasters_option['total-school' . '_' . $scheme . '_hover']) . "
	}
	/* Finish Highlight Color */
	
	
	/* Start Heading Color */
	{$rule}#lp-navigation .nav-link .course-item-title:before, 
	{$rule}#lp-navigation .nav-link .course-item-title, 
	{$rule}.learn-press-search-course-form .search-course-button:before, 
	{$rule}.user-basic-info .user-nicename, 
	{$rule}.single-lp_course .learn-press-course-results-progress .number, 
	{$rule}.single-lp_course .learn-press-course-results-progress .percentage-sign,
	{$rule}.quiz-questions-list .question-title, 
	{$rule}.single-lp_course .course-meta .course-price:before, 
	{$rule}.single-lp_course .course-meta .course-students:before, 
	{$rule}.single-lp_course .course-meta .learn-press-course-status:before, 
	{$rule}.single-lp_course .course-meta .course-author:before, 
	{$rule}#learn-press-course-curriculum .course-item.course-lesson:before, 
	{$rule}#learn-press-course-curriculum .course-item.course-quiz:before,
	{$rule}.course-students-list .students .name,
	{$rule}.single-quiz .quiz-sidebar .quiz-countdown #quiz-countdown-value,
	{$rule}.archive .lp_course .cmsmasters_lrp_archive_content .cmsmasters_lrp_archive_item_title,
	{$rule}.learn-press-user-profile .learn-press-tabs > li.current > a,
	{$rule}.learn-press-user-profile .learn-press-tabs > li > a:hover,
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab.active a,
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab a:hover,
	{$rule}.quiz-result-mark,
	{$rule}.quiz-result .quiz-result-summary .quiz-result-field > label,
	{$rule}#quiz-countdown {
		" . cmsmasters_color_css('color', $cmsmasters_option['total-school' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}.learn-press-content-item-title.content-item-quiz-title .quiz-countdown:before,
	{$rule}.learn-press-content-item-title.content-item-quiz-title .quiz-countdown:after,
	{$rule}.lp-group-heading-title .toggle-icon:before,
	{$rule}.lp-group-heading-title .toggle-icon:after,
	{$rule}#learn-press-course-curriculum .section-header .meta .collapse:before,
	{$rule}#learn-press-course-curriculum .section-header .meta .collapse:after {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['total-school' . '_' . $scheme . '_heading']) . "
	}
	
	{$rule}#learn-press-block-content:before {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['total-school' . '_' . $scheme . '_heading']) . "
	}
	/* Finish Heading Color */
	
	
	/* Start Alternate Background Color */
	{$rule}.learn-press-content-item-summary .learn-press-question-explanation, 
	{$rule}.learn-press-content-item-summary .learn-press-question-hint,
	{$rule}.learn-press-pagination .page-numbers li .page-numbers, 
	{$rule}.learn-press-content-item-title .lp-expand, 
	{$rule}.quiz-result .quiz-result-summary .quiz-result-field, 
	{$rule}#learn-press-course-curriculum .course-item.viewable:hover, 
	{$rule}#learn-press-course-curriculum .section-header, 
	{$rule}.course-rate .review-bar,
	{$rule}.lp-course-progress .lp-progress-bar,
	{$rule}.learn-press .message,
	{$rule}.learn-press .lp-message,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .img_placeholder,
	{$rule}.question-hint-wrap,
	{$rule}.single-quiz .quiz-question-nav .lp-question-wrap ul,
	{$rule}.quiz-result-mark {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['total-school' . '_' . $scheme . '_alternate']) . "
	}
	/* Finish Alternate Background Color */
	
	
	/* Start Main Background Color */
	{$rule}#lp-navigation .nav-link .course-item-title:hover:before,
	{$rule}#popup_container #popup_title,
	{$rule}.learn-press-checkout-review-order-table thead,
	{$rule}.learn-press-cart-table thead,
	{$rule}.order_details thead,
	{$rule}.table-orders thead,
	{$rule}.quiz-history thead th,
	{$rule}.lp-label,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_price,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_free {
		" . cmsmasters_color_css('color', $cmsmasters_option['total-school' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}#course-curriculum-popup #popup-main, 
	{$rule}#course-curriculum-popup #popup-sidebar, 
	{$rule}body.content-item-only .learn-press-content-item-only, 
	{$rule}#lp-navigation .nav-link .course-item-title:before, 
	{$rule}#lp-navigation, 
	{$rule}.learn-press-question-options > li.answer-option label p.single-lines:before, 
	{$rule}.learn-press-pagination .page-numbers li .page-numbers:hover, 
	{$rule}.learn-press-pagination .page-numbers li .page-numbers.current, 
	{$rule}.learn-press-courses li.course, 
	{$rule}#learn-press-course-curriculum .course-item.item-current:before, 
	{$rule}#learn-press-course-curriculum .course-item.course-lesson:before, 
	{$rule}#learn-press-course-curriculum .course-item.course-quiz:before,
	{$rule}#learn-press-course-curriculum .section-header:hover, 
	{$rule}.learn-press-notice, 
	{$rule}.learn-press-message, 
	{$rule}.learn-press-error,
	{$rule}.question-explanation-content,
	{$rule}.question-hint-content,
	{$rule}.lp-course-progress .lp-progress-value > span, 
	{$rule}.learn-press-user-profile .learn-press-tabs > li.current > a, 
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab.active a,
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_footer, 
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .lpr_course_inner,
	{$rule}.single-quiz .quiz-question-nav .lp-question-wrap, 
	{$rule}.curriculum-sections .section-content > li {
		" . cmsmasters_color_css('background-color', $cmsmasters_option['total-school' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.lp-course-progress .lp-progress-value > span:after {
		" . cmsmasters_color_css('border-top-color', $cmsmasters_option['total-school' . '_' . $scheme . '_bg']) . "
	}
	
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab.active a,
	{$rule}.learn-press-user-profile .learn-press-tabs > li.current > a {
		" . cmsmasters_color_css('border-bottom-color', $cmsmasters_option['total-school' . '_' . $scheme . '_bg']) . "
	}
	/* Finish Main Background Color */
	
	
	/* Start Borders Color */
	{$rule}button[disabled] {
		" . cmsmasters_color_css('color', $cmsmasters_option['total-school' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}#lp-navigation .nav-link .course-item-title:before, 
	{$rule}#lp-navigation, 
	{$rule}.learn-press-question-options > li.answer-option label p.single-lines:before, 
	{$rule}button[disabled]:hover, 
	{$rule}.quiz-questions-list .question-title, 
	{$rule}.learn-press-courses li.course, 
	{$rule}.learn-press-content-item-title .lp-expand, 
	{$rule}.quiz-result .quiz-result-summary .quiz-result-field, 
	{$rule}#learn-press-course-curriculum .course-item.course-lesson:before, 
	{$rule}#learn-press-course-curriculum .course-item.course-quiz:before,
	{$rule}#learn-press-course-curriculum .section-header, 
	{$rule}.learn-press-user-profile .learn-press-tabs, 
	{$rule}.learn-press-tabs .learn-press-nav-tabs, 
	{$rule}.learn-press-wishlist-courses li, 
	{$rule}.learn-press-checkout-review-order-table tbody tr, 
	{$rule}.learn-press-checkout-review-order-table tfoot tr, 
	{$rule}.learn-press-cart-table tbody tr,
	{$rule}.order_details tbody tr,
	{$rule}.table-orders tbody tr,
	{$rule}.order_details tfoot tr,
	{$rule}.table-orders tfoot tr,
	{$rule}.quiz-history tbody tr,
	{$rule}.question-explanation-content,
	{$rule}.question-hint-content,
	{$rule}.learn-press-notice, 
	{$rule}.learn-press-message, 
	{$rule}.learn-press-error,
	{$rule}.lp-course-progress .lp-progress-value > span, 
	{$rule}.complete-lesson-button.completed, 
	{$rule}#learn_press_payment_form, 
	{$rule}.course-content-lesson-nav-text, 
	{$rule}#user_quizzes .quiz-result-mark, 
	{$rule}#user_courses > h3, 
	{$rule}.learn-press .message, 
	{$rule}.learn-press .lp-message, 
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .cmsmasters_course_footer, 
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .lpr_course_inner, 
	{$rule}.cmsmasters_learnpress_shortcode .lpr_course_post .img_placeholder, 
	{$rule}.archive .lp_course .cmsmasters_lrp_archive_content .cmsmasters_lrp_archive_item, 
	{$rule}.single-quiz .quiz-question-nav .lp-question-wrap > h4, 
	{$rule}.single-quiz .quiz-question-nav .lp-question-wrap, 
	{$rule}.single-quiz .quiz-result .quiz-result-summary .quiz-result-field, 
	{$rule}.single-quiz .quiz-questions > h3, 
	{$rule}.single-quiz .quiz-summary .entry-title, 
	{$rule}.curriculum-sections .section-content > li,
	{$rule}.course-reviews-list > li,
	{$rule}.course-review-head,
	{$rule}.course-curriculum-title,
	{$rule}.cmsmasters_course_title,
	{$rule}.course-curriculum-title,
	{$rule}.students-list-title,
	{$rule}.learn-press-content-item-title,
	{$rule}.course-lesson-heading,
	{$rule}.lp-course-progress-heading {
		" . cmsmasters_color_css('border-color', $cmsmasters_option['total-school' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab.active a,
	{$rule}.learn-press-user-profile .learn-press-tabs > li.current > a {
		" . cmsmasters_color_css('border-left-color', $cmsmasters_option['total-school' . '_' . $scheme . '_border']) . "
	}
	
	{$rule}.learn-press-tabs .learn-press-nav-tabs .learn-press-nav-tab.active a,
	{$rule}.learn-press-user-profile .learn-press-tabs > li.current > a {
		" . cmsmasters_color_css('border-right-color', $cmsmasters_option['total-school' . '_' . $scheme . '_border']) . "
	}
	/* Finish Borders Color */

/***************** Finish {$title} LearnPress Color Scheme Rules ******************/

";
	}
	
	
	return $custom_css;
}

add_filter('total_school_theme_colors_secondary_filter', 'total_school_learnpress_colors');

